<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountValidatorTest extends TestCase
{

    /**
     * @test
     */
    public function can_access_api_route()
    {
        $response = $this->json('POST','/api/account',[
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function validate_account_existence()
    {
        $response = $this->json('POST','/api/account',[
            'email' => 'xyz@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertJson([
            'emailexistence' => false
        ]);
    }

    /**
     * @test
     */
    public function validate_email_empty()
    {
        $response = $this->json('POST','/api/account',[
            'email' => '',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertJson([
            'emailexistence' => true
        ]);
    }

    /**
     * @test
     */
    public function validate_password_matched()
    {
        $response = $this->json('POST','/api/account',[
            'email' => 'xyz@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertJson([
            'matched' => true
        ]);
    }

    /**
     * @test
     */
    public function validate_password_empty()
    {
        $response = $this->json('POST','/api/account',[
            'email' => 'xyz@gmail.com',
            'password' => '',
            'password_confirmation' => ''
        ]);

        $response->assertJson([
            'matched' => false
        ]);
    }

    /**
     * @test
     */
    public function can_get_correct_error_message(){


        $response = $this->json('POST', '/api/account',[
            'email' => 'xyz@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '123456789'
        ]);

        $response->assertJson([
            'message' => ['password'=>[
                'The password confirmation does not match.'
                ]
            ]
        ]);

        $response = $this->json('POST', '/api/account',[
            'email' => 'xyz@gmail.com',
            'password' => '1234',
            'password_confirmation' => '123456789'
        ]);

        $response->assertJson([
            'message' => ['password'=>[
                'The password confirmation does not match.',
                'The password must be at least 8 characters.'
                ]
            ]
        ]);
    }
}
