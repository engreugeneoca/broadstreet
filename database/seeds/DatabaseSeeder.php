<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory('App\User', 1)->create([
            'firstname' => 'Admin',
            'lastname' => 'Mike',
            'email' => 'mike@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'), // password
            'remember_token' => Str::random(10),
        ]);
        factory('App\User', 1)->create([
            'firstname' => 'Admin',
            'lastname' => 'Stacey',
            'email' => 'stacey@broadstreetvaluations.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'), // password
            'remember_token' => Str::random(10),
        ]);
        factory('App\User', 10)->create();
        factory('App\User', 10)->create();

        factory('App\Role', 1)->create(['name'=>'administrator']);
        factory('App\Role', 1)->create(['name'=>'appraiser']);
        factory('App\Role', 1)->create(['name'=>'borrower']);
        factory('App\Role', 1)->create(['name'=>'client']);
        factory('App\Role', 1)->create(['name'=>'salesagent']);
        factory('App\Role', 1)->create(['name'=>'assigner']);
        factory('App\Role', 1)->create(['name'=>'assigning']);
        factory('App\Role', 1)->create(['name'=>'csr']);
        factory('App\Role', 1)->create(['name'=>'qualitycontrol']);
        factory('App\Role', 1)->create(['name'=>'qualityassurance']);

        // Populate the user_role table
        $roles = App\Role::all();
        App\User::all()->each(function ($user) use ($roles) { 
            if($user->firstname == "Admin"){
                $user->roles()->attach(1);
            }else{
                $user->roles()->attach(rand(2,10));
            }
        });
    }
}
