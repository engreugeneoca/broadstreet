<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class UserController extends Controller
{
    function edit_user(Request $request){
        if($request->eaccountid==""){
            return redirect('/dashboard/manage-user');
        }

        if($request->eaccountid){
            // edit this account

            $criteria = [
                'efirstname'         => 'required',
                'elastname'          => 'required',
                'eemail'             => 'required|email',
                'erole'             => 'required'
            ];
    
            $request->validate($criteria);

            $passwordcriteria = [
                'epassword' => 'required|string|min:8|confirmed'
            ];

            $user = User::find($request->eaccountid);
            $user->firstname = $request->efirstname;
            $user->lastname = $request->elastname;
            if($request->epassword){
                $request->validate($passwordcriteria,['epassword.confirmed'=> "The password confirmation does not match."]);
                $user->password = Hash::make($request->epassword);
            }
            $user->roles()->detach($user->roles()->first()->id);
            $user->roles()->attach(Role::whereName($request->erole)->first());
            $user->update();
            return redirect('/dashboard/manage-user');
        }
    }

    function delete_user(Request $request){
        $user = User::find($request->accountid);
        $user->delete();
        return redirect('dashboard/manage-user');
    }

    function logout(Request $request) {
        Auth::logout();
        $request->session()->invalidate();
        Session::flush();
        return redirect('/');
    }
}
