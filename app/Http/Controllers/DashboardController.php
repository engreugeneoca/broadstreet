<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    function index()
    {
        $admin_name = Auth::user()->firstname . " " . Auth::user()->lastname;
        $account = Auth::user();
        return view('dashboard.dash')->with(['admin_name'=>$admin_name, 'account'=>$account]);
    }

    function panel_users(){
        $users = User::orderBy('firstname', 'asc')->paginate(5);
        $account = Auth::user();
        return view('page.manage-user')->with(['users'=>$users, 'account'=>$account]);
    }
}
