<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerificationEmailNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification(){
        $this->notify(new VerificationEmailNotification());
    }

    public function markEmailAsVerified(){
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    public function hasVerifiedEmail()
    {
        return ! is_null($this->email_verified_at);
    }

    public function roles(){
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    public function hasRole($role){
        if($this->roles()->first()->name==$role){
            return true;
        }else{
            return false;
        }
    }

    public function getRole(){
        $rolesTable = [
            'administrator'=>'Administrator',
            'appraiser'=>'Appraiser',
            'borrower'=>'Borrower',
            'client'=>'Client',
            'salesagent'=>'Sales Agent',
            'assigner'=> 'Assigner',
            'assigning'=> 'Assigning',
            'csr'=>'CSR',
            'qualitycontrol'=>'Quality Control',
            'qualityassurance'=>'Quality Assurance',
        ];

        return $rolesTable[$this->roles()->first()->name];
    }

    public function getId(){
        return $this->id;
    }

}
