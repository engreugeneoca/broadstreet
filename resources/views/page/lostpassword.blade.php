@extends ('layout')

@section('content')
	

 


                    <!-- card -->
                    <div class="card">
                      <div class="card-header text-left">
                      {{ __('Reset Password') }}
                      </div>
                      <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                         <p>Please enter your username or email address. You will receive a link to create a new password via email.</p>

                           <form class="ml-5 mr-5"  method="POST" action="{{ route('password.email') }}">
                           @csrf
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mb-2" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <button class="btn btn-brown">Send Password Reset Link</button>
                           </form> 

                        
                        
                      </div>
                    </div> <!-- ./card --> 

 
	

@stop