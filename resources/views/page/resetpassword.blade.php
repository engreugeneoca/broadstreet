@extends ('layout')

@section('content')
	

 


                    <!-- card -->
                    <div class="card">
                      <div class="card-header text-left">
                      {{ __('Reset Password') }}
                      </div>
                      <div class="card-body">
                      @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                         <p>Please enter your New Password</p>

                           <form class="ml-5 mr-5"  method="POST" action="{{ route('password.update') }}">
                           @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mb-2" name="email" value="{{ $email ?? old('email') }}" required placeholder="Email" autocomplete="email" autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} mb-2" name="password" required placeholder="New Password" autocomplete="new-password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <input id="password-confirm" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} mb-2" name="password_confirmation" required placeholder="Retype Password" autocomplete="new-password">
                                <button class="btn btn-brown">{{ __('Reset Password') }}</button>
                           </form> 

                        
                        
                      </div>
                    </div> <!-- ./card --> 

 
	

@stop