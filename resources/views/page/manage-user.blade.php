@extends ('layouts.master')

@section('content')


<div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="dash-inner-content">

                <div class="dash-inner-title">
                    <strong> List of Users</strong>  
                </div>
                    
                <div class="p-3">
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $key => $row)
                                <tr>
                                <th scope="row">{{$users->firstItem()+$key}}</th>
                                    <td id="firstname-{{$row['id']}}">{{$row['firstname']}}</td>
                                    <td id="lastname-{{$row['id']}}">{{$row['lastname']}}</td>
                                    <td id="email-{{$row['id']}}">{{$row['email']}}</td>
                                    <td id="role-{{$row['id']}}">{{$row->getRole()}}</td>
                                    <td><button class="btn btn-success btn-sm" data-toggle="modal" data-target="#edituser-modal" id="btn-edit-{{$row['id']}}">Edit</button>
                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteuser-modal"  id="btn-delete-{{$row['id']}}">Delete</button>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table><!-- ./table --> 
                    </div><!-- ./table-responsive -->

                    <div class="d-flex justify-content-between align-items-center">
                            <button class="btn btn-brown" data-toggle="modal" data-target="#adduser-modal">Add</button>
                            <!-- pagination -->
                            {{$users->links()}}
                            <!-- ./pagination -->
                    </div>
                </div> <!-- ./p-3 -->

                 

            </div>      
            
                      
        </div>
    </div><!-- ./row -->


    <!--  add-modal  -->
    <div class="modal fade" id="adduser-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <div class="p-3">    
                    <form method="POST" action="{{ route('register') }}" id="registerform">
                    @csrf
                    @if (($message = Session::get('error')))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>{{$message}}</strong>
                        </div> 
                    @endif
                
                    @if (count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach  
                            </ul>
                        </div>  
                    @endif
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>{{ __('First Name') }}</label>
                                <input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Last Name') }}</label>
                                <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('E-Mail Address') }}</label>
                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <div class="registeremail"></div>
                            </div>
                            <div class="form-group col-md-6">
                            <label>{{ __('Role') }}</label>
                                <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required autocomplete="role">
                                    <option value="administrator">Administrator</option>
                                    <option value="appraiser">Appraiser</option>
                                    <option value="borrower">Borrower</option>
                                    <option value="client">Client</option>
                                    <option value="salesagent">Sales Agent</option>
                                    <option value="assigner">Assigner</option>
                                    <option value="assigning">Assigning</option>
                                    <option value="csr">CSR</option>
                                    <option value="qualitycontrol">Quality Control</option>
                                    <option value="qualityassurance">Quality Assurance</option>
                                    
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('Password') }}</label>
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" required autocomplete="new-password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label>{{ __('Confirm Password') }}</label>
                                <input type="password" class="form-control" id="passwordconfirm" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            <div class="form-group col-md-12 registerpassword"></div>
                        </div>   
                    
                </div><!-- ./p-3 -->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-brown" id="btn-adduser">Add User</button>
            </form><!-- ./form -->
        </div>
        </div>
    </div>
    </div>
    <!-- ./add-modal -->

    <!--  edit-modal  -->
    <div class="modal fade" id="edituser-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="p-3">    
                <form method="POST" action="{{url('/user/edituser')}}" id="editform">
                @csrf
                        <input type="hidden" id="eaccountid" name="eaccountid" value="">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>{{ __('First Name') }}</label>
                                <input id="efirstname" type="text" class="form-control{{ $errors->has('efirstname') ? ' is-invalid' : '' }}" name="efirstname" required autocomplete="efirstname" autofocus>
                                @if ($errors->has('efirstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('efirstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Last Name') }}</label>
                                <input id="elastname" type="text" class="form-control{{ $errors->has('elastname') ? ' is-invalid' : '' }}" name="elastname" required autocomplete="elastname" autofocus>
                                @if ($errors->has('elastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('elastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('E-Mail Address') }}</label>
                                <input id="eemail" type="email" class="form-control{{ $errors->has('eemail') ? ' is-invalid' : '' }}" name="eemail" readonly autocomplete="eemail">
                                @if ($errors->has('eemail'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('eemail') }}</strong>
                                    </span>
                                @endif
                                <div class="editemail"></div>
                            </div>
                            <div class="form-group col-md-6">
                            <label>{{ __('Role') }}</label>
                                <select id="erole" class="form-control{{ $errors->has('erole') ? ' is-invalid' : '' }}" name="erole" required autocomplete="erole">
                                    <option value="administrator">Administrator</option>
                                    <option value="appraiser">Appraiser</option>
                                    <option value="borrower">Borrower</option>
                                    <option value="client">Client</option>
                                    <option value="salesagent">Sales Agent</option>
                                    <option value="assigner">Assigner</option>
                                    <option value="assigning">Assigning</option>
                                    <option value="csr">CSR</option>
                                    <option value="qualitycontrol">Quality Control</option>
                                    <option value="qualityassurance">Quality Assurance</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('New Password') }}</label>
                                <input id="epassword" type="password" class="form-control{{ $errors->has('epassword') ? ' is-invalid' : '' }}" name="epassword" autocomplete="new-epassword">
                                @if ($errors->has('epassword'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('epassword') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Confirm New Password') }}</label>
                                <input id="epassword-confirm" type="password" class="form-control" name="epassword_confirmation" autocomplete="new-epassword">
                            </div>

                            <div class="form-group col-md-12 editpassword"></div>
                        </div>   
                        
                    
                    </div><!-- ./p-3 -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-brown" id="btn-save" disabled>SAVE CHANGES</button>
            </form><!-- ./form -->        
        </div>
        </div>
    </div>
    </div>
    <!-- ./edit-modal -->


    <!--  delete-modal  -->
    <div class="modal fade" id="deleteuser-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete this user?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <form id="delete-form" action="{{ url('/user/deleteuser') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="accountid" id="deleteid" value="">
                    <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    <!-- ./delete-modal -->




@stop 