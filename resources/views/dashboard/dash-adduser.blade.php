
@extends ('dash-layout')

@section('content')

<div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="dash-sidebar">
                <h5>Users</h5>
                <ul class="list-group dash-list">
                    <li class="list-group-item"><a href="/dashboard/users"><i class="fa fa-users"></i> List of Users</a></li>
                    <li class="list-group-item active"><a href="/dashboard/adduser"><i class="fa fa-plus"></i> Add User</a></li>
                    <li class="list-group-item"><a href="/dashboard/edituser"><i class="fa fa-edit"></i> Edit User</a></li>
                    <li class="list-group-item"><a href="/dashboard/deleteuser"><i class="fa fa-close"></i> Delete User</a></li>
                </ul>                 
            </div><!-- ./dash-sidebar-->

        </div>    

        <div class="col-md-9 col-lg-10">
            <div class="dash-inner-content">

                <div class="dash-inner-title">
                    <strong>Add User</strong>  
                </div>


                <div class="p-3">    
                    <form method="POST" action="{{ route('register') }}">
                    @csrf
                    @if (($message = Session::get('error')))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>{{$message}}</strong>
                        </div> 
                    @endif
                
                    @if (count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach  
                            </ul>
                        </div>  
                    @endif
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>{{ __('First Name') }}</label>
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Last Name') }}</label>
                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-2">
                            <label>{{ __('Role') }}</label>
                                <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required autocomplete="role">
                                    <option value="lender">Lender</option>
                                    <option value="appraiser">Appraiser</option>
                                    <option value="administrator">Administrator</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="new-password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>   
                        <button type="submit" class="btn btn-brown">ADD USER</button>
                    </form><!-- ./form -->
                </div><!-- ./p-3 -->
                
            </div>                
        </div>
    </div><!-- ./row -->


@stop 