
@extends ('dash-layout')

@section('content')

<div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="dash-sidebar">
                <h5>Welcome, {{$admin_name}} </h5>
                <ul class="list-group dash-list">
                    <li class="list-group-item active"><a href="#"><i class="fa fa-users"></i> Link</a></li>
                    <li class="list-group-item"><a href="#"><i class="fa fa-plus"></i> Link</a></li>
                    <li class="list-group-item"><a href="#"><i class="fa fa-edit"></i> Link</a></li>
                    <li class="list-group-item"><a href="#"><i class="fa fa-close"></i>  Link</a></li>

                </ul>                 
            </div><!-- ./dash-sidebar-->

        </div>    

        <div class="col-md-9 col-lg-10">
            <div class="dash-inner-content">

                <div class="dash-inner-title">
                    <strong>Orders</strong>  
                </div>
                    
                <div class="p-3">
                   
                </div> <!-- ./p-3 -->

            </div>                
        </div>
    </div><!-- ./row -->


@stop 