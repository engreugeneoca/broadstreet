
@extends ('dash-layout')

@section('content')

<div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="dash-sidebar">
                <h5>Users</h5>
                <ul class="list-group dash-list">
                    <li class="list-group-item"><a href="/dashboard/users"><i class="fa fa-users"></i> List of Users</a></li>
                    <li class="list-group-item"><a href="/dashboard/adduser"><i class="fa fa-plus"></i> Add User</a></li>
                    <li class="list-group-item active"><a href="/dashboard/edituser"><i class="fa fa-edit"></i> Edit User</a></li>
                    <li class="list-group-item"><a href="/dashboard/deleteuser"><i class="fa fa-close"></i> Delete User</a></li>
                </ul>                 
            </div><!-- ./dash-sidebar-->

        </div>    

        <div class="col-md-9 col-lg-10">
            <div class="dash-inner-content">

                <div class="dash-inner-title">
                    <strong>Edit User</strong>  
                </div>


                <div class="p-3">    
                <form method="POST" action="{{url('/user/edituser')}}">
                @csrf
                        <input type="hidden" id="accountid" name="accountid" value="">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>{{ __('First Name') }}</label>
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" required autocomplete="firstname" autofocus>
                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Last Name') }}</label>
                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" required autocomplete="lastname" autofocus>
                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" readonly autocomplete="email">
                            </div>
                            <div class="form-group col-md-2">
                            <label>{{ __('Role') }}</label>
                                <select id="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required autocomplete="role">
                                    <option value="lender">Lender</option>
                                    <option value="appraiser">Appraiser</option>
                                    <option value="administrator">Administrator</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">  
                            <div class="form-group col-md-6">
                                <label>{{ __('New Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" autocomplete="new-password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{ __('Confirm New Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                            </div>
                        </div>   
                        <button type="submit" class="btn btn-brown" id="btn-save" disabled>SAVE CHANGES</button>
                    </form><!-- ./form -->
                </div><!-- ./p-3 -->

                 <hr>

                <div class="p-3">
                <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $id=1; ?>
                            @foreach($users as $row)
                                <tr>
                                    <th scope="row">{{$id}}</th>
                                    <td id="firstname-{{$row['id']}}">{{$row['firstname']}}</td>
                                    <td id="lastname-{{$row['id']}}">{{$row['lastname']}}</td>
                                    <td id="email-{{$row['id']}}">{{$row['email']}}</td>
                                    <td id="role-{{$row['id']}}">{{$row->getRole()}}</td>
                                    <td><button class="btn btn-outline-success btn-sm" id="btn-edit-{{$row['id']}}"><i class="fa fa-edit"></i> Edit</button></td>
                                </tr>
                                <?php $id++; ?>
                            @endforeach
                        </tbody>
                    </table><!-- ./table -->
                    
                </div> <!-- ./p-3 -->

            </div>                
        </div>
    </div><!-- ./row -->


@stop 