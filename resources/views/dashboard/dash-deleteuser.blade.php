
@extends ('dash-layout')

@section('content')

<div class="row">
        <div class="col-md-3 col-lg-2">
            <div class="dash-sidebar">
                <h5>Users</h5>
                <ul class="list-group dash-list">
                    <li class="list-group-item"><a href="/dashboard/users"><i class="fa fa-users"></i>  List of Users</a></li>
                    <li class="list-group-item"><a href="/dashboard/adduser"><i class="fa fa-plus"></i>  Add User</a></li>
                    <li class="list-group-item"><a href="/dashboard/edituser"> <i class="fa fa-edit"></i>  Edit User</a></li>
                    <li class="list-group-item active"><a href="/dashboard/deleteuser"><i class="fa fa-close"></i>  Delete User</a></li>
                </ul>                 
            </div><!-- ./dash-sidebar-->

        </div>    

        <div class="col-md-9 col-lg-10">
            <div class="dash-inner-content">

                <div class="dash-inner-title">
                    <strong>Delete Users</strong>  
                </div>
                    
                <div class="p-3">
                <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $id=1; ?>
                            @foreach($users as $row)
                                <tr>
                                    <th scope="row">{{$id++}}</th>
                                    <td>{{$row['firstname']}}</td>
                                    <td>{{$row['lastname']}}</td>
                                    <td>{{$row['email']}}</td>
                                    <td>{{$row->getRole()}}</td>
                                    <td>
                                        
                                        <form id="logout-form" action="{{ url('/user/deleteuser') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="accountid" value="{{$row['id']}}">
                                            <button class="btn btn-outline-danger btn-sm" type="submit"><i class="fa fa-close"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table><!-- ./table --> 
                    
                </div> <!-- ./p-3 -->

            </div>                
        </div>
    </div><!-- ./row -->


@stop 