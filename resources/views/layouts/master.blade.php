<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" >

    <!-- fontawesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" >

    <title>Broad Street Valuations</title>
  </head>
  <body class="dash-body">
    <!--
    <h1>Dashboard</h1> 

    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form> -->


    <div class="container-fluid">

        <nav class="navbar navbar-expand-lg navbar-light  dash-nav">
            <a class="navbar-brand" href="{{route('home')}}">Broad Street Valuations, Inc.</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Order</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Reports</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Products</a>
                </li>
                @if($account->hasRole('administrator'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/dashboard/manage-user')}}">Manage</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="#">Search</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Open Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Assigning Followup</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">QC Page</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ url('/user/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" style="display: none;">
                @csrf
                </form>
                </li>
                </ul>
            </div>
            </nav>
        <!-- ./nav-dash -->


       
<div class="dash-content">

    @yield('content')

</div><!-- ./dash-content-->

    

    
   <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"> </script>
    <script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"> </script>

  </body>
</html>